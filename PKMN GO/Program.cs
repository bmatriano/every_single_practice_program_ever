﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//	Matriano, BrennanY.
//	152805	
//	11/11/16
//	I	have	not	discussed	the	C#	language	code	in	my	program		
//	with	anyone	other	than	my	instructor	or	the	teaching	assistants	assigned	to	this	
//	course.	I	have	not	used	C#	language	code	obtained	from		
//	another	person,	or	any	other	unauthorized	source,	either	modified	or	unmodified.
//	Any	C#	language	code	or	documentation	used	in	my	program		
//	that	was	obtained	from	another	source,	such	as	a	text	book,	a	web	page,	
//	or	another	person,	have	been	clearly	noted	with	proper	citation	in	the		
//	comments	of	my	program.
namespace PKMN_GO
{
    class Program
    {
        static void Main(string[] args)
        {
            int failCount = 1;
            Boolean guessed=false;
            int guessNumber=50;
            int guessMax=100;
            int guessMin=0;
            int guessCount = 0;
            string Reg_pokemonName;
            string Reg_pokemonType;
            string Reg_pokemonEvolution;
            int Reg_pokemonCost;
            int Reg_pokemonMultiplier;
            string Catch_pokemonName;
            int Catch_pokemonHP;
            double Catch_catchChance;
            int Catch_pokemonCP;
            int Transfer_transferID;
            Console.Title="POKEMON CO";
            
            Trainer user = new Trainer();
        Start:
                
                Boolean ChoiceActuallyMakesSenseMenu = false;
                ChoiceActuallyMakesSenseMenu = false;
                Console.WriteLine("=====POKEMON CO=====");
                Console.WriteLine("1 for Register");
                Console.WriteLine("2 for Catch");
                Console.WriteLine("3 for Transfer");
                Console.WriteLine("4 for Evolve");
                Console.WriteLine("5 for View Bag");
                Console.WriteLine("6 for View PokeDex");
                Console.WriteLine("7 for Store");
                Console.WriteLine("8 to battle against a random registered Pokemon");
                Console.WriteLine("9 to heal Pokemon");
                Console.WriteLine("10 to battle with a random Pokemon then try to capture it");
                Console.WriteLine("11 to view your stats as a trainer");
                Console.WriteLine("0 to QUIT");
                Console.WriteLine("Enter choice:");
                
                while (ChoiceActuallyMakesSenseMenu==false)
                {
                    string menuChoice = Console.ReadLine();
                    switch (menuChoice)
                    {
                        case "1":
                    RegStart:
                            try
                            {
                                Console.Clear();
                                ChoiceActuallyMakesSenseMenu = true;
                                Console.WriteLine("===REGISTER====");
                                Console.Write("Name:");
                                Reg_pokemonName = Console.ReadLine();
                                if (user.ExistCheck(Reg_pokemonName))
                                {
                                    Console.WriteLine("POKEMON IS ALREADY REGISTERED");
                                    Console.ReadLine();
                                    Console.Clear();
                                    goto Start;
                                }
                                else
                                {
                                    Console.Write("Type:");
                                    Reg_pokemonType = Console.ReadLine();
                                    Console.Write("Evolution:");
                                    Reg_pokemonEvolution = Console.ReadLine();
                                    if (Reg_pokemonEvolution.Equals("none", StringComparison.OrdinalIgnoreCase))
                                    {
                                        user.PokemonAddtoDex(Reg_pokemonName, Reg_pokemonType, "none", 10, 1);
                                        Console.ReadLine();
                                        Console.Clear();
                                        goto Start;
                                    }
                                    else
                                    {
                                        Console.Write("Cost:");
                                        Reg_pokemonCost = Int32.Parse(Console.ReadLine());
                                        Console.Write("Multiplier:");
                                        Reg_pokemonMultiplier = Int32.Parse(Console.ReadLine());
                                        user.PokemonAddtoDex(Reg_pokemonName, Reg_pokemonType, Reg_pokemonEvolution, Reg_pokemonCost, Reg_pokemonMultiplier);
                                        Console.ReadLine();
                                        Console.Clear();
                                        goto Start;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Something went wrong.");
                                Console.ReadLine();
                                Console.Clear();
                                goto Start;
                            }                     
                                
                        case "2":
                    CatchStart:
                            Console.Clear();
                            ChoiceActuallyMakesSenseMenu = true;
                            try
                            {
                                Console.WriteLine("===CATCH====");
                                Console.WriteLine("You have {0} balls", user.checkBalls());
                                Console.Write("Name:");
                                Catch_pokemonName = Console.ReadLine();
                                if (user.ExistCheck(Catch_pokemonName)==false)
                                {
                                    Console.WriteLine("POKEMON DOESN'T EXIST");
                                    Console.ReadLine();
                                    Console.Clear();
                                    goto Start;
                                }
                                Console.Write("HP:");
                                Catch_pokemonHP = Int32.Parse(Console.ReadLine());
                                Console.Write("CP:");
                                Catch_pokemonCP = Int32.Parse(Console.ReadLine());

                            Catch:
                                Catch_catchChance = user.catchChanceGenerator();
                                Console.Clear();
                                Console.WriteLine("===CATCH====");
                                Console.WriteLine("You have {0} balls", user.checkBalls());
                                Console.WriteLine("Name:{0}", Catch_pokemonName);
                                Console.WriteLine("HP:{0}", Catch_pokemonHP);
                                Console.WriteLine("CP:{0}", Catch_pokemonCP);
                                if (user.ExistCheck(Catch_pokemonName) && user.checkBalls() > 0 && Catch_catchChance >= 0.2)
                                {
                                    Console.WriteLine("{0} HAS BEEN CAUGHT!", Catch_pokemonName);
                                    user.PokemonCatch(Catch_pokemonName, Catch_pokemonHP, Catch_pokemonCP);
                                    Console.ReadLine();
                                    Console.Clear();
                                    goto Start;
                                }
                                else if (Catch_catchChance < 0.2-user.CatchFleeChanceModifier()/17*Catch_pokemonCP/100 && user.ExistCheck(Catch_pokemonName))
                                {
                                    user.catchFail();
                                    if (user.fleeChanceGenerator() > 0.30-user.CatchFleeChanceModifier()/17*Catch_pokemonCP/75)
                                    {
                                        Console.WriteLine("CATCH FAILED! TRY AGAIN? [Y/N]");
                                        string catchContinue = Console.ReadLine();
                                        if (catchContinue.Equals("y", StringComparison.OrdinalIgnoreCase))
                                        {
                                            goto Catch;
                                        }
                                        else
                                        {
                                            Console.Clear();
                                            goto Start;
                                        }
                                    }
                                    else {
                                        Console.WriteLine("IT RAN AWAY :(((");
                                        Console.ReadLine();
                                        Console.Clear();
                                        goto Start;
                                    }
                                }
                                else if (user.checkBalls() <= 0)
                                {
                                    Console.WriteLine("YOU DON'T HAVE THE BALLS TO CATCH IT :P");
                                    Console.ReadLine();
                                    Console.Clear();
                                    goto Start;
                                }
                                else
                                {
                                    Console.WriteLine("THAT POKEMON DOES NOT EXIST");
                                    Console.ReadLine();
                                    Console.Clear();
                                    goto Start;
                                }
                            }
                            catch (Exception e) {
                                goto CatchStart;
                            }
                        case "3":
                            ChoiceActuallyMakesSenseMenu = true;
                            Console.Clear();
                            Console.WriteLine("====TRANSFER====");
                            user.PrintAllOnHand();
                            try
                            {
                                Console.Write("Enter ID: ");
                                Transfer_transferID = Int32.Parse(Console.ReadLine());
                                Console.Clear();
                                user.turnPokemonToCandy(Transfer_transferID);
                                Console.ReadLine();
                                Console.Clear();
                                goto Start;
                            }
                            catch(Exception e){
                                Console.WriteLine("INVALID ID");
                                Console.ReadLine();
                                Console.Clear();
                                goto Start;
                            }
                        case "4":
                            ChoiceActuallyMakesSenseMenu = true;
                            Console.Clear();
                            Console.WriteLine("====EVOLVE====");
                            user.PrintAllOnHand();
                            try
                            {
                                Transfer_transferID = Int32.Parse(Console.ReadLine());
                                Console.Clear();
                                user.Evolve(Transfer_transferID);
                                Console.ReadLine();
                                Console.Clear();
                                goto Start;
                            }
                            catch(Exception e){
                                Console.WriteLine("INVALID ID");
                                Console.ReadLine();
                                goto Start;
                            }
                        case "5":
                            ChoiceActuallyMakesSenseMenu = true;
                            Console.WriteLine("====VIEW BAG====");
                            user.PrintAllOnHand();
                            Console.ReadLine();
                            Console.Clear();
                            goto Start;
                        case "6":
                            ChoiceActuallyMakesSenseMenu = true;
                            user.PrintAllPokeDex();
                            Console.ReadLine();
                            Console.Clear();
                            goto Start;
                        case "7":
                            Console.Clear();
                            user.StoreMenu();
                            ChoiceActuallyMakesSenseMenu = true;
                            Console.Clear();
                            goto Start;
                        case "8":
                            ChoiceActuallyMakesSenseMenu = true;
                            user.randomBattle();
                            goto Start;
                        case "9":
                            user.PokemonCenter();
                            Console.Clear();
                            Console.WriteLine("All your pokemon are fully healed. Go Battle!");
                            Console.ReadLine();
                            ChoiceActuallyMakesSenseMenu = true;
                            goto Start;
                        case "10":
                            ChoiceActuallyMakesSenseMenu = true;
                            user.randomBattleWithCatch();
                            goto Start;
                        case "11":
                            Console.Clear();
                            user.PrintTrainerStats();
                            Console.ReadLine();
                            goto Start;
                        case "0":
                            ChoiceActuallyMakesSenseMenu = true;
                            Environment.Exit(0);
                            break;
                        default:
                            failCount++;
                            Console.Clear();
                            if (failCount <= 3) goto Start;
                            else {
                                
                                Console.WriteLine("Let's play another game then, which guesses a numebr you're thinking of using binary search.");
            BackupGameStart:
                                Console.WriteLine("Set the minimum");
                                guessMin=Int32.Parse(Console.ReadLine());
                                Console.WriteLine("Set the maximum");
                                guessMax = Int32.Parse(Console.ReadLine());
                                int range = guessMax - guessMin;
                                int MaxGuess=0;
                                while(range!=1){
                                    range /= 2;
                                    MaxGuess++;                                                                
                                }
                                Console.WriteLine("I'll find it in {0} guesses or less", MaxGuess);
                                guessNumber = (guessMax + guessMin) / 2;
                                while (guessed==false && guessCount<=MaxGuess) {
                                    Console.WriteLine("Is it " + guessNumber +"?");
                                    String HighLow = Console.ReadLine();
                                    if(HighLow=="higher"){
                                        guessMin = guessNumber;
                                        guessNumber = (guessMin+guessMax)/2;
                                    }
                                    else if (HighLow == "lower")
                                    {
                                        guessMax = guessNumber;
                                        guessNumber = (guessMax+guessMin)/ 2;
                                    }
                                    else if (HighLow == "yes") {
                                        Console.WriteLine("Yay!");
                                        Console.WriteLine("Earned  1 PokeBall and 100 Credits");
                                        Console.ReadLine();
                                        user.binarySearchPrize();
                                        guessed = true;
                                        Console.WriteLine("Play Again?");
                                        if (Console.ReadLine() == "yes") goto BackupGameStart;
                                        else goto Start;
                                    }

                                    guessCount++;
                                    if (guessCount >= MaxGuess) {
                                        Console.WriteLine("Either we guessed it na, or you didn't think of a number :((((");
                                        Console.WriteLine("Play Again?");
                                        if (Console.ReadLine() == "yes") goto BackupGameStart;
                                        else goto Start;

                                    }
                                } 
                            }
                            break;
                           
                            
                    }

                }
        }
    }
}


              
