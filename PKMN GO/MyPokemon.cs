﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PKMN_GO
{

    //	Matriano, BrennanY.
    //	152805	
    //	11/11/16
    //	I	have	not	discussed	the	C#	language	code	in	my	program		
    //	with	anyone	other	than	my	instructor	or	the	teaching	assistants	assigned	to	this	
    //	course.	I	have	not	used	C#	language	code	obtained	from		
    //	another	person,	or	any	other	unauthorized	source,	either	modified	or	unmodified.
    //	Any	C#	language	code	or	documentation	used	in	my	program		
    //	that	was	obtained	from	another	source,	such	as	a	text	book,	a	web	page,	
    //	or	another	person,	have	been	clearly	noted	with	proper	citation	in	the		
    //	comments	of	my	program.
    class MyPokemon
    {
        String MyPokemon_name;
        public int MyPokemon_HP;
        public int MyPokemon_CP;
        public int MyPokemon_bagSlot;
        int MyPokemon_currentHP;
        Boolean MyPokemon_inBag;
        public MyPokemon(int slot, string name, int HP, int CP, bool inBag) {
            MyPokemon_CP = CP;
            MyPokemon_HP = HP;
            MyPokemon_name = name;
            MyPokemon_bagSlot = slot;
            MyPokemon_inBag = inBag;
            MyPokemon_currentHP = HP;
        }
        public MyPokemon(string name, int HP, int CP)
        {
            MyPokemon_CP = CP;
            MyPokemon_HP = HP;
            MyPokemon_name = name;
        }
        public String returnName() {
            return MyPokemon_name;
        }
        public int returnHP()
        {
            return MyPokemon_HP;
        }
        public int returnCP()
        {
            return MyPokemon_CP;
        }
        public int returnBagSlot() {
            return MyPokemon_bagSlot;
        }
        public int returnCurrentHP() {
            return MyPokemon_currentHP;
        }
        public void takeDamage(int dam) {
            MyPokemon_currentHP -= dam;
        }

        public void Evolve(String evolution,int multiplier) {
            MyPokemon_name = evolution;
            MyPokemon_CP *= multiplier;
            MyPokemon_HP *= multiplier;
        }
        public void Heal()
        {
            MyPokemon_currentHP = MyPokemon_HP;
        }
    }
}
