﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//	Matriano, BrennanY.
//	152805	
//	11/11/16
//	I	have	not	discussed	the	C#	language	code	in	my	program		
//	with	anyone	other	than	my	instructor	or	the	teaching	assistants	assigned	to	this	
//	course.	I	have	not	used	C#	language	code	obtained	from		
//	another	person,	or	any	other	unauthorized	source,	either	modified	or	unmodified.
//	Any	C#	language	code	or	documentation	used	in	my	program		
//	that	was	obtained	from	another	source,	such	as	a	text	book,	a	web	page,	
//	or	another	person,	have	been	clearly	noted	with	proper	citation	in	the		
//	comments	of	my	program.
namespace PKMN_GO
{
    class Trainer
    {
        private int Trainer_level = 1;
        private int max = -1;
        private int Trainer_exp = 0;
        private int Trainer_neededEXP = 10;
        private int PokemonID = 1;
        private int Trainer_bagSlots = 10; 
        private int Trainer_bagPos = 1;
        public int Trainer_balls = 10;
        private int Trainer_swag = 1000;
        private Boolean upgrade1Owned = false;
        private Boolean upgrade2Owned = false;
        private Boolean upgrade3Owned = false;
        private Boolean upgrade4Owned = false;
        public Boolean Trainer_foundPokemon = false;
        List<PokemonRegInfo> PokemonRegistered = new List<PokemonRegInfo>();
        MyPokemon[] PokemonOwned = new MyPokemon[10];
        public void PokemonAddtoDex(string name, string type, string evolution, int cost, int multiplier)
        {
            PokemonRegistered.Add(new PokemonRegInfo(PokemonID, name, type, evolution, cost, multiplier));
            PokemonID++;
            Console.WriteLine("POKEMON SUCCESSFULLY REGISTERED");
            AddTrainerEXP(5);
        }
        public void PokemonCatch(string name, int HP, int CP)
        {

            if (Trainer_bagSlots > 0)
            {
                max++;
                PokemonOwned[max] = new MyPokemon(Trainer_bagPos, name, HP, CP, true);
                Trainer_bagSlots--;
                Trainer_bagPos++;
                Trainer_balls--;
                for (int i = 0; i < PokemonRegistered.Count(); i++)
                {
                    if (PokemonOwned[max].returnName().Equals(PokemonRegistered[i].ReturnName(), StringComparison.OrdinalIgnoreCase))
                    {
                        PokemonRegistered[i].CatchCandy();
                    }
                }
                AddTrainerEXP(10);
            }
            else
            {
                PokemonOwned[max] = new MyPokemon(Trainer_bagPos, name, HP, CP, true);
                Console.WriteLine("POKEMON SENT TO PC.");
                Trainer_balls--;
            }
        }
        public Boolean ExistCheck(String name)
        {
            for (int i = 0; i < PokemonRegistered.Count(); i++)
            {
                if (PokemonRegistered[i].PokemonRegInfo_name.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return true;

                }

            }
            return false;
        }
        public void PrintAllPokeDex()
        {
            Console.WriteLine("====VIEW POKEDEX====");
            Console.WriteLine("{0,5}{1,10}{2,10}", "INDEX", "NAME", "EVOLUTION");
            if (PokemonRegistered.Count <= 0)
            {
                Console.WriteLine("YOUR POKEDEX IS EMPTY.");
                Console.WriteLine("====================");
                Console.WriteLine("TOTAL REGISTERED: {0}", PokemonRegistered.Count);
            }
            else
            {
                for (int i = 0; i < PokemonRegistered.Count(); i++)
                {
                    //int gap1 = 15;
                    //int gap2 = 10;
                    //Console.Write(PokemonRegistered[i].ReturnPokemonID());
                    //for (int j = gap1 - PokemonRegistered[i].ReturnPokemonID().ToString().Length + PokemonRegistered[i].ReturnName().ToString().Length; j > 0; j--)
                    //{
                    //    Console.Write(" ");
                    //}
                    //Console.Write(PokemonRegistered[i].ReturnName());
                    //for (int k = gap2 - PokemonRegistered[i].ReturnType().ToString().Length; k > 0; k--)
                    //{
                    //    Console.Write(" ");
                    //}
                    //if (PokemonRegistered[i].ReturnEvolution().Equals("none", StringComparison.OrdinalIgnoreCase) != true)
                    //{
                    //    Console.Write(PokemonRegistered[i].ReturnEvolution());
                    //}
                    //else
                    //{
                    //    Console.Write("------");
                    //}
                    //Console.Write("    ");
                    //Console.Write("\n");
                    if(PokemonRegistered[i].ReturnEvolution()=="none"){
                        Console.WriteLine("{0,5}{1,10}{2,10}",i+1,PokemonRegistered[i].ReturnName(),"----");
                    }
                    else
                    {
                        Console.WriteLine("{0,5}{1,10}{2,10}", i + 1, PokemonRegistered[i].ReturnName(), PokemonRegistered[i].ReturnEvolution());
                    }
                }
                Console.WriteLine("====================");
                Console.WriteLine("TOTAL REGISTERED: {0}", PokemonRegistered.Count);
            }

        }
        public void PrintAllOnHand()
        {
            Console.WriteLine("{0,5} {1,10} {2,5} {3,5} {4,10}", "ID", "NAME", "HP", "CP", "CURRENT HP");
            if (Trainer_bagSlots == 10)
            {
                Console.WriteLine("YOUR BAG IS EMPTY.");
                Console.WriteLine("============");
                Console.WriteLine("BAG CAPACITY: {0}/10", Trainer_bagSlots);
            }
            else
            {
                int id = 1;
                for (int i = 0; i <= max + 1; i++)
                {
                    if (PokemonOwned[i] != null)
                    {

                        Console.WriteLine("{0,5} {1,10} {2,5} {3,5} {4,10}", id, PokemonOwned[i].returnName(), PokemonOwned[i].returnHP(), PokemonOwned[i].returnCP(), PokemonOwned[i].returnCurrentHP());
                        id++;
                        //Console.Write(id);
                        //for (int j = 15 - PokemonOwned[i].returnName().Length + Trainer_bagPos.ToString().Length; j > 0; j--)
                        //{
                        //    Console.Write(" ");
                        //}
                        //Console.Write(PokemonOwned[i].returnName());
                        //for (int k = 10 - PokemonOwned[i].returnHP().ToString().Length; k > 0; k--)
                        //{
                        //    Console.Write(" ");
                        //}
                        //Console.Write(PokemonOwned[i].returnHP());
                        //for (int k = 5 - PokemonOwned[i].returnCP().ToString().Length; k > 0; k--)
                        //{
                        //    Console.Write(" ");
                        //}
                        //Console.Write(PokemonOwned[i].returnCP());
                        //for (int k = 5 - PokemonOwned[i].returnCP().ToString().Length; k > 0; k--)
                        //{
                        //    Console.Write(" ");
                        //}
                        //Console.Write(PokemonOwned[i].returnCurrentHP());
                        //Console.Write("    ");
                        //Console.Write("\n");
                    }
                    else
                    {
                        continue;
                    }
                }
                Console.WriteLine("============");
                Console.WriteLine("BAG CAPACITY: {0}/10", Trainer_bagSlots);
                for (int i = 0; i < PokemonRegistered.Count(); i++)
                {
                    Console.WriteLine("{0} CANDY: {1}", PokemonRegistered[i].ReturnName(), PokemonRegistered[i].CandyOnHand);
                }
                Console.WriteLine("CREDITS: {0}", Trainer_swag);
            }


        }
        public void PrintTrainerStats()
        {
            Console.WriteLine("Level:"+Trainer_level);
            Console.WriteLine("You have" +Trainer_exp+"/"+Trainer_neededEXP+" EXP needed to level up.");
        }
        public void turnPokemonToCandy(int transferID) //work on rhis
        {
            transferID--;
            if (PokemonOwned[transferID] != null)
            {
                Trainer_swag += PokemonOwned[transferID].returnCP();
                Console.WriteLine("{0} TRANSFERRED, {1} CREDITS EARNED", PokemonOwned[transferID].returnName(), PokemonOwned[transferID].returnCP());
                for (int i = 0; i < PokemonRegistered.Count(); i++)
                {
                    if (PokemonOwned[transferID].returnName().Equals(PokemonRegistered[i].ReturnName(), StringComparison.OrdinalIgnoreCase))
                    {
                        PokemonRegistered[i].AddCandy();
                        Trainer_bagSlots++;
                    }
                }
                for (int i = transferID; i <= max+1; i++)
                {
                    PokemonOwned[i] = PokemonOwned[i + 1];
                }
                PokemonOwned[max] = null;
                max--;
                AddTrainerEXP(10);
            }
            else
            {
                Console.WriteLine("INVALID ID");
            }
        }
        public void AddTrainerEXP(int EXPchange)
        {
            Trainer_exp += EXPchange;
            Console.WriteLine("Earned {0} trainer EXP", EXPchange);
            if (Trainer_exp >= Trainer_neededEXP)
            {
                Trainer_level++;
                Trainer_exp -= Trainer_neededEXP;
                Trainer_neededEXP *= 2;
            }
        }
        public int CatchFleeChanceModifier()
        {
            int mod = 0;
            if (upgrade1Owned)
            {
                mod = 2;
            }
            if (upgrade2Owned)
            {
                mod += 1;
            }
            int CatchChance = Trainer_level + mod / 2;
            return CatchChance;
        }
        public void Evolve(int evolveID)
        {
            evolveID--;
            for (int i = 0; i < PokemonRegistered.Count(); i++)
            {
                if (PokemonRegistered[i].PokemonRegInfo_name == PokemonOwned[evolveID].returnName())
                {
                    if (PokemonRegistered[i].ReturnEvolution().Equals("none", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine("{0} CANNOT BE EVOLVED FURTHER", PokemonRegistered[i].PokemonRegInfo_name);
                        break;
                    }
                    else if (PokemonRegistered[i].ReturnCandyOnHand() < PokemonRegistered[i].ReturnCost())
                    {
                        Console.WriteLine("YOU DO NOT HAVE ENOUGH CANDY {0}/{1}", PokemonRegistered[i].ReturnCandyOnHand(), PokemonRegistered[i].ReturnCost());
                        break;
                    }
                    else
                    {
                        Console.WriteLine("{0} has evolved into {1}", PokemonRegistered[i].PokemonRegInfo_name, PokemonRegistered[i].ReturnEvolution());
                        PokemonOwned[evolveID].Evolve(PokemonRegistered[i].ReturnEvolution(), PokemonRegistered[i].ReturnMultiplier());
                        PokemonRegistered[i].performEvolution();
                    DoesFurtherEvolve:
                        Console.WriteLine("Does it evolve further?[true/false]");
                        try
                        {
                            bool doesEvolve = Boolean.Parse(Console.ReadLine());
                            if (doesEvolve == false)
                            {
                                PokemonRegistered.Add(new PokemonRegInfo(PokemonID, PokemonOwned[evolveID].returnName(), PokemonRegistered[i].ReturnType(), "none", 1, 1));
                                PokemonID++;
                                AddTrainerEXP(15);
                            }
                            else
                            {
                                Console.Write("Evolution:");
                                String EvolveReg_pokemonEvolution = Console.ReadLine();
                                Console.Write("Type:");
                                String EvolveReg_pokemonType = Console.ReadLine();
                                Console.Write("Cost:");
                                int EvolveReg_pokemonCost = Int32.Parse(Console.ReadLine());
                                Console.Write("Multiplier:");
                                int EvolveReg_pokemonMultiplier = Int32.Parse(Console.ReadLine());
                                PokemonRegistered.Add(new PokemonRegInfo(PokemonID, PokemonRegistered[i].ReturnEvolution(), PokemonRegistered[i].ReturnType(), EvolveReg_pokemonEvolution, EvolveReg_pokemonCost, EvolveReg_pokemonMultiplier));
                                PokemonID++;
                                AddTrainerEXP(20);
                                break;
                            }
                        }
                        catch (Exception e)
                        {
                            goto DoesFurtherEvolve;
                        }

                    }
                }

            }

        }
        public int checkBalls()
        {
            return Trainer_balls;
        }
        public Double catchChanceGenerator()
        {
            Random random = new Random();
            return random.NextDouble();
        }
        public Double fleeChanceGenerator()
        {
            Random anotherRandom = new Random();
            return anotherRandom.NextDouble();
        }
        public void catchFail()
        {
            Trainer_balls--;
        }
        public void binarySearchPrize()
        {
            Trainer_balls++;
            Trainer_swag += 100;
        }
        public void StoreMenu()
        {
            Console.WriteLine("[1] 1 PokeBall FOR 100 CREDITS");
            Console.WriteLine("[2] 3 PokeBalls FOR 250 CREDITS");
            Console.WriteLine("[3] 5 PokeBalls FOR 400 CREDITS");
            if (upgrade1Owned == false)
            {
                Console.WriteLine("[4] Upgrade to Great Balls for 500");
            }
            if (upgrade2Owned == false)
            {
                Console.WriteLine("[5] Upgrade to Ultra Balls for 1000");
            }
        storeStart:
            int storeChoice = Int32.Parse(Console.ReadLine());
            switch (storeChoice)
            {
                case 1:
                    if (Trainer_swag >= 100)
                    {
                        Trainer_swag -= 100;
                        Trainer_balls++;
                        Console.WriteLine("YOU NOW HAVE 1 MORE BALL");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH CREDITS");
                    }
                    break;
                case 2:
                    if (Trainer_swag >= 250)
                    {
                        Trainer_swag -= 250;
                        Trainer_balls += 3;
                        Console.WriteLine("YOU NOW HAVE 3 MORE BALLS");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH CREDITS");
                    }
                    break;
                case 3:
                    if (Trainer_swag >= 400)
                    {
                        Trainer_swag -= 400;
                        Console.WriteLine("YOU NOW HAVE 5 MORE BALLS");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH CREDITS");
                    }
                    break;
                case 4:
                    if (Trainer_swag >= 500)
                    {
                        Console.WriteLine("CATCH CANCE UPGRADED");
                        Trainer_swag -= 1000;
                        upgrade1Owned = true;
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH CREDITS");
                    }
                    break;
                case 5:
                    if (Trainer_swag >= 400 && upgrade1Owned == true)
                    {
                        Console.WriteLine("CATCH CANCE UPGRADED");
                        Trainer_swag -= 400;
                        upgrade2Owned = true;
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH CREDITS");
                    }
                    break;
                default:
                    goto storeStart;
                    Console.ReadLine();
            }
        }
        public void PokemonCenter()
        {
            for (int i = 0; i <= max; i++)
            {
                PokemonOwned[i].Heal();
            }
        }
        public void randomBattleWithCatch()
        {
            if (PokemonRegistered.Count() == 0)
            {
                Console.WriteLine("NO REGISTERED POKEMON. REGISTER FIRST");

            }
            else
            {
                Random generationRange = new Random();
                Random cpRand = new Random();
                Random hpRand = new Random();
                bool youWin = false;
                bool youLose = false;
                MyPokemon opponent = new MyPokemon(PokemonRegistered[generationRange.Next(0, PokemonRegistered.Count() - 1)].ReturnName(), hpRand.Next(1, 300), cpRand.Next(1, 390));
                int opponentCurrHP = opponent.returnHP();
                Console.Clear();
                Console.WriteLine("====Random Battle====");
                Console.WriteLine("This is your opponent");
                Console.WriteLine("Name: " + opponent.returnName());
                Console.WriteLine("HP: " + opponent.returnHP());
                Console.WriteLine("CP: " + opponent.returnCP());
                Console.WriteLine("==== CHOOSE YOUR POKEMON ====");
                PrintAllOnHand();
                Console.Write("ENTER ID:");
                int select = Int32.Parse(Console.ReadLine());
                Console.Clear();
                if (PokemonOwned[select - 1].returnCurrentHP() <= 0)
                {
                    Console.WriteLine("Your Pokemon fainted! Heal First.");
                }
                else
                {
                    while ((PokemonOwned[select - 1].returnCurrentHP() > 0) || (opponentCurrHP > 0))
                    {
                        if ((PokemonOwned[select - 1].returnCurrentHP() <= 0) || (opponentCurrHP <= 0))
                        {
                            if (PokemonOwned[select - 1].returnCurrentHP() <= 0)
                            {
                                youLose = true;
                                break;
                            }
                            else
                            {
                                youWin = false;
                                break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("==== Opponent ====");
                            Console.WriteLine("Name: " + opponent.returnName());
                            Console.WriteLine("HP: " + opponentCurrHP);
                            Console.WriteLine("==== Yours ====");
                            Console.WriteLine("Name: " + PokemonOwned[select - 1].returnName());
                            Console.WriteLine("HP: " + PokemonOwned[select - 1].returnCurrentHP());
                            Console.WriteLine("Enter: Rock, Paper, Scissors, or anything else to try to catch it.");
                            string userChoice = Console.ReadLine();
                            string computerChoice = "";
                            Random RockPaperScissors = new Random();
                            Console.Clear();
                            if (RockPaperScissors.Next(1, 10) >= 3 && RockPaperScissors.Next(1, 10) <= 6)
                            {
                                computerChoice = "Rock";
                            }
                            else if (RockPaperScissors.Next(1, 10) < 3)
                            {
                                computerChoice = "Paper";
                            }
                            else if (RockPaperScissors.Next(1, 10) > 6)
                            {
                                computerChoice = "Scissors";
                            }
                            else { 
                            
                            }
                            if (computerChoice.Equals("rock", StringComparison.OrdinalIgnoreCase))
                            {
                                if (userChoice.Equals("Rock", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("Both: Rock. TIE. DAMAGE TO BOTH");
                                    opponentCurrHP -= 10;
                                    PokemonOwned[select - 1].takeDamage(10);
                                }
                                else if (userChoice.Equals("Scissors", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("You: Scissors. Opponent:Rock. YOU LOSE. HEAVY DAMAGE TO YOUR POKEMON");
                                    PokemonOwned[select - 1].takeDamage(20);
                                }
                                else if (userChoice.Equals("Paper", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("You: Paper. Opponent:Rock. OPPONENT DAMAGED! KEEP IT UP.");
                                    opponentCurrHP -= 20;
                                }
                                else {
                                    if (catchChanceGenerator() > opponent.returnCurrentHP() / opponent.returnHP())
                                    {
                                        PokemonCatch(opponent.returnName(), opponent.returnHP(), opponent.returnCP());
                                        Console.WriteLine("OPPONENT CAPTURED!");
                                        Console.ReadLine();
                                        break;
                                    }
                                    else {
                                        Console.WriteLine("Catch failed! Your Pokemon is damaged!");
                                        Console.ReadLine();
                                        PokemonOwned[select - 1].takeDamage(20);
                                    }
                                }
                            }
                            else if (computerChoice.Equals("scissors", StringComparison.OrdinalIgnoreCase))
                            {
                                if (userChoice.Equals("Rock", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("You: Rock (pun intended) Opponent: Scissors. YOU WIN! KEEP IT UP.");
                                    opponentCurrHP -= 20;
                                }
                                else if (userChoice.Equals("Scissors", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("Both: Rock (but you rock more jk). It's a tie! Both Pokemon damaged.");
                                    opponentCurrHP -= 10;
                                    PokemonOwned[select - 1].takeDamage(10);
                                }
                                else if (userChoice.Equals("Paper", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("You: Paper. Opponent:Scissors. Ouch.");
                                    PokemonOwned[select - 1].takeDamage(20);
                                }
                                else
                                {
                                    if (catchChanceGenerator() > opponent.returnCurrentHP() / opponent.returnHP())
                                    {
                                        PokemonCatch(opponent.returnName(), opponent.returnHP(), opponent.returnCP());
                                        Console.WriteLine("OPPONENT CAPTURED!");
                                        Console.ReadLine();
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Catch failed! Your Pokemon is damaged!");
                                        Console.ReadLine();
                                        PokemonOwned[select - 1].takeDamage(20);
                                    }
                                }
                            }
                            else
                            {
                                if (userChoice.Equals("Paper", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("Both: Paper. Both of you get hurt.");
                                    opponentCurrHP -= 10;
                                    PokemonOwned[select - 1].takeDamage(10);
                                }
                                else if (userChoice.Equals("Scissors", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("You: Scissors. Opponent: Paper. You know what happens.");
                                    opponentCurrHP -= 20;
                                }
                                else if (userChoice.Equals("Rock", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("You: Rock. Oppponent:Paper. Not a good idea, eh?");
                                    PokemonOwned[select - 1].takeDamage(20);
                                }
                                else
                                {
                                    if (catchChanceGenerator() > opponent.returnCurrentHP() / opponent.returnHP())
                                    {
                                        PokemonCatch(opponent.returnName(), opponent.returnHP(), opponent.returnCP());
                                        Console.WriteLine("OPPONENT CAPTURED!");
                                        Console.ReadLine();
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Catch failed! Your Pokemon is damaged!");
                                        PokemonOwned[select - 1].takeDamage(20);
                                    }
                                }
                            }

                        }
                    }
                    if ((youWin == true) && (youLose = false))
                    {
                        Console.WriteLine("You win! Earned 100 Credits and a Pokeball");
                        binarySearchPrize();
                    }
                    else if ((youWin == false) && (youLose = true))
                    {
                        Console.WriteLine("G E T   R E K T   B O Y");
                    }
                    else
                    {
                        Console.WriteLine("It's liver! (It's atay)");
                    }
                    Console.WriteLine();
                    Console.Clear();
                }
            }
        }
        public void randomBattle()
        {
            if (PokemonRegistered.Count() == 0)
            {
                Console.WriteLine("NO REGISTERED POKEMON. REGISTER FIRST");

            }
            else
            {
                Random generateRandomIndex = new Random();
                Random generateCP = new Random();
                Random generateHP = new Random();
                bool youWin = false;
                bool youLose = false;
                MyPokemon opponent = new MyPokemon(PokemonRegistered[generateRandomIndex.Next(0, PokemonRegistered.Count() - 1)].ReturnName(), generateHP.Next(1, 100), generateCP.Next(1, 90));
                int opponentCurrHP = opponent.returnHP();
                Console.Clear();
                Console.WriteLine("This is your opponent");
                Console.WriteLine("Name: " + opponent.returnName());
                Console.WriteLine("HP: " + opponent.returnHP());
                Console.WriteLine("CP: " + opponent.returnCP());
                Console.WriteLine("==== CHOOSE YOUR POKEMON ====");
                PrintAllOnHand();
                Console.Write("ENTER ID:");
                int select = Int32.Parse(Console.ReadLine());
                Console.Clear();
                if (PokemonOwned[select - 1].returnCurrentHP() <= 0)
                {
                    Console.WriteLine("Your Pokemon fainted! Heal First.");
                }
                else
                {
                    while ((PokemonOwned[select - 1].returnCurrentHP() > 0) || (opponentCurrHP > 0))
                    {
                        if ((PokemonOwned[select - 1].returnCurrentHP() <= 0) || (opponentCurrHP <= 0))
                        {
                            if (PokemonOwned[select - 1].returnCurrentHP() <= 0)
                            {
                                youLose = true;
                                break;
                            }
                            else
                            {
                                youWin = false;
                                break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("==== Opponent ====");
                            Console.WriteLine("Name: " + opponent.returnName());
                            Console.WriteLine("HP: " + opponentCurrHP);
                            Console.WriteLine("==== Yours ====");
                            Console.WriteLine("Name: " + PokemonOwned[select - 1].returnName());
                            Console.WriteLine("HP: " + PokemonOwned[select - 1].returnCurrentHP());
                            Console.WriteLine("Enter: Rock, Paper, Scissors");
                            string userChoice = Console.ReadLine();
                            string computerChoice = "";
                            Random RockPaperScissors = new Random();
                            Console.Clear();
                            if (RockPaperScissors.Next(1, 10) >= 3 && RockPaperScissors.Next(1, 10) <= 6)
                            {
                                computerChoice = "Rock";
                            }
                            else if (RockPaperScissors.Next(1, 10) < 3)
                            {
                                computerChoice = "Paper";
                            }
                            else if (RockPaperScissors.Next(1, 10) > 6)
                            {
                                computerChoice = "Scissors";
                            }
                            if (computerChoice.Equals("rock", StringComparison.OrdinalIgnoreCase))
                            {
                                if (userChoice.Equals("Rock",StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("Both: Rock. TIE. DAMAGE TO BOTH");
                                    opponentCurrHP -= 10;
                                    PokemonOwned[select - 1].takeDamage(10);
                                }
                                else if (userChoice.Equals("Scissors", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("You: Scissors. Opponent:Rock. YOU LOSE. HEAVY DAMAGE TO YOUR POKEMON");
                                    PokemonOwned[select - 1].takeDamage(20);
                                }
                                else
                                {
                                    Console.WriteLine("OPPONENT DAMAGED! KEEP IT UP.");
                                    opponentCurrHP -= 20;
                                }
                            }
                            else if (computerChoice.Equals("scissors", StringComparison.OrdinalIgnoreCase))
                            {
                                if (userChoice.Equals("Rock", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("WIN! KEEP GOING");
                                    opponentCurrHP -= 20;
                                }
                                else if (userChoice.Equals("Scissors", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("TIE");
                                    opponentCurrHP -= 10;
                                    PokemonOwned[select - 1].takeDamage(10);
                                }
                                else
                                {
                                    Console.WriteLine("LOSE");
                                    PokemonOwned[select - 1].takeDamage(20);
                                }
                            }
                            else
                            {
                                if (userChoice.Equals("Paper", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("TIE");
                                    opponentCurrHP -= 10;
                                    PokemonOwned[select - 1].takeDamage(10);
                                }
                                else if (userChoice.Equals("Scissors", StringComparison.OrdinalIgnoreCase))
                                {
                                    Console.WriteLine("WIN!");
                                    opponentCurrHP -= 20;
                                }
                                else
                                {
                                    Console.WriteLine("LOSE");
                                    PokemonOwned[select - 1].takeDamage(20);
                                }
                            }

                        }
                    }
                    if ((youWin == true) && (youLose = false))
                    {
                        Console.WriteLine("You win! Earned 100 Credits and a Pokeball");
                        binarySearchPrize();
                    }
                    else if ((youWin == false) && (youLose = true))
                    {
                        Console.WriteLine("G E T   R E K T   B O Y");
                    }
                    else
                    {
                        Console.WriteLine("It's liver! (It's atay)");
                    }
                    Console.WriteLine();
                    Console.Clear();
                }
            }
        }
    }

}