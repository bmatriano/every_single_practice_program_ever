﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PKMN_GO
{

    //	Matriano, BrennanY.
    //	152805	
    //	11/11/16
    //	I	have	not	discussed	the	C#	language	code	in	my	program		
    //	with	anyone	other	than	my	instructor	or	the	teaching	assistants	assigned	to	this	
    //	course.	I	have	not	used	C#	language	code	obtained	from		
    //	another	person,	or	any	other	unauthorized	source,	either	modified	or	unmodified.
    //	Any	C#	language	code	or	documentation	used	in	my	program		
    //	that	was	obtained	from	another	source,	such	as	a	text	book,	a	web	page,	
    //	or	another	person,	have	been	clearly	noted	with	proper	citation	in	the		
    //	comments	of	my	program.
    class PokemonRegInfo
    {
        private int PokemonID;
        public String PokemonRegInfo_name;
        private string Type;
        private string Evolution;
        private int Cost;
        private int Multiplier;
        public int CandyOnHand=0;
        public PokemonRegInfo(int ID, string name, string type, string evolution, int cost, int multiplier) {
            PokemonID = ID;
            Type = type;
            Evolution = evolution;
            Cost = cost;
            Multiplier = multiplier;
            PokemonRegInfo_name = name;
            CandyOnHand = 0;
        }
        public String ReturnName() {
            return PokemonRegInfo_name;
        }
        public String ReturnEvolution()
        {
            return Evolution;
        }
        public String ReturnType()
        {
            return Type;
        }
        public int ReturnCost() {
            return Cost;
        }
        public int ReturnCandyOnHand() {
            return CandyOnHand;
        }
        public int ReturnMultiplier()
        {
            return Multiplier;
        }
        public int ReturnPokemonID()
        {
            return PokemonID;
        }
        public void AddCandy(){
            CandyOnHand++;
        }
        public void CatchCandy(){
            CandyOnHand += 3;
        }
        public void performEvolution() {
            CandyOnHand -= Cost;
        }
    }
}
